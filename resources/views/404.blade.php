@extends('layouts.app')
@section('content')
    <div class="container-fluid app-body">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Opps!</div>
                    <div class="panel-body text-center">
                        <h3>
                            404 Not Found
                            <br>
                            This page is out of date. Please go back to dashboard
                        </h3>
                        <h6 class="text-center">
                            <a href="/" class="btn btn-primary btn-lg">Dashboard</a>
                        </h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
