<?php

namespace Bulkly\Http\Controllers\Auth;

use Bulkly\User;
use Bulkly\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;
use Mail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'tos' => 'required',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['first_name'].' '.$data['last_name'],
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }


    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function register(Request  $request)
    {
        $input = $request->all();
        $validator = $this->validator($input);



        if($validator->passes()){

            $data = $this->create($input)->toArray();


            $data['verification_token'] = str_random(25);

            $user = User::find($data['id']);
            $user->verification_token = $data['verification_token'];
            $user->save();
            
            /*
            Mail::send('mails.confirmation', $data, function($message) use ($data){
                $message->to($data['email']);
                $message->subject(ucwords($data['first_name']).', let\'s confirm your Bulkly account');
            });
            */
            
            try{
                
                $client = new Client;
                
                $result = $client->request('POST', 'https://api2.autopilothq.com/v1/contact', [
                    'headers' => [
                        'autopilotapikey' => env('AUTOP'),
                        'Content-Type'     => 'application/json'
                    ],
                    'json' => [
                        'contact' => [
                            'FirstName' => $user->first_name,
                            'LastName' =>$user->last_name,
                            'Email' =>$user->email,
                            'custom' => [
                                'string--Confirmation--Url' => route('confirmation', $user->verification_token),
                                ],
                            '_autopilot_list' => '9ECC7B84-9EB3-43EB-8C08-72A20E2573EA'
                        ]
                    ]
                ]);
            } catch (RequestException $e) {
                
            } catch (ClientException $e) {
                
            }
            

            return redirect(route('login'))->with('status', 'A confirmation email has been sent. Please check your inbox.');
        }


        return redirect(route('register'))->with('errors', $validator->messages())->withInput($request->input());
    }



    public function UpdateTempUser(Request  $request)
    {
        print_r($request->all());

    }







}
